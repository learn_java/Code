package com.iweb.lesson07;

public class Test {
    //String
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        String str = "A";
        for (int i = 0; i <100000 ; i++) {
            str +="A";

        }
        long end = System.currentTimeMillis();
        System.out.println(end-start);

        start = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder("A");
        for (int i = 0; i <100000 ; i++) {
           sb.append("A");

        }
        end = System.currentTimeMillis();
        System.out.println(end-start);
        System.out.println(str.equals(sb.toString()));
    }
}
