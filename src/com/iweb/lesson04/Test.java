package com.iweb.lesson04;

public class Test {

    public static void main(String[] args) {

        int i =100;//无方法可以调用
        //引用数据类型，Integer是一个类 Integer i= new....
        Integer a =100;//jdk给包装类提供了自动装箱和自动拆箱的机制所用可以使用Integer

        Integer b =100;
        int result = a +b;
        System.out.println(result);
    }
}
