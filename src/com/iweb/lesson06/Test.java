package com.iweb.lesson06;

public class Test {
    //不可变字符串String
//    public static void main(String[] args) {
//        String word = "hello";
//
//       String newWord =  word.concat("java");
//
//        System.out.println(word);
//        System.out.println(newWord);
//    }

    //可变的字符串 StringBuffer  StringBuilder
    public static void main(String[] args) {

        StringBuilder sb = new StringBuilder("hello");
        StringBuilder newSb = sb.append("java");

        System.out.println(sb);
        System.out.println(newSb);


    }
}
