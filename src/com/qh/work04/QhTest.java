package com.qh.work04;

import java.util.Random;

public class QhTest {

    public String verificationCode(){
        //定义一个获取字符的字符串
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        //保存得到字符的字符串
        String Code = "";
        Random random = new Random();//创建对象
        //得到字符串的长度
        int range = str.length();
        //做6次循环，得到一个6位的字符串
        for (int i = 0; i < 6; i++) {
            //得到索引对应的字符，并进行拼接
            Code +=(str.charAt(random.nextInt(range)));//random.nextInt(range)从0~range随机产生一个数
        }
        return Code;//返回字符串

    }

    public static void main(String[] args) {
        //创建类的一个对象
        QhTest qhTest = new QhTest();
        //打印字符串
        System.out.println("验证码："+qhTest.verificationCode());
    }

}
