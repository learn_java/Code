package com.qh.work03;

import java.util.Calendar;
import java.util.Scanner;

public class QhTest {

    int time;

    public double pay() {
        double pay ;
        //获取当前时间
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_YEAR);// 每年第几天
        int hour = calendar.get(Calendar.HOUR_OF_DAY);// 小时
        int minute = calendar.get(Calendar.MINUTE);// 分钟
       // System.out.print(minute);

        //8:00-22:00 2毛/分
        if(8<hour+time/60 && time/60+hour<=22){
            return time*0.2;
        }
        //22:00-次日8:00 1毛/分
        else if(22<hour+time/60 && hour+time/60<24 || 0<=hour+time/60 && hour+time/60<=8){
            return time*0.1;
        }
        else if(8<hour && hour<=22 && hour+time/60>22 || hour+time/60>24){
            return (hour+time/60-22)*0.1+(22-hour)*0.2;
        }
        else if (0<hour && hour<8 && hour+time/60>8 ){
            return (8-hour )*0.1+(hour+time/6-8)*0.2;
        }
        else return 0.0;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        QhTest test = new QhTest();//创建一个对象
        System.out.println("输入通话时间分钟：");
        test.time = sc.nextInt(); //得到输入通话的时间

        System.out.println("总共通话"+test.time+"分钟,共花费"+test.pay()+"元");

    }
}
