package com.qh.work02;


public class QhTest {
    public static String[] strReplace(String str) {
        /*
         * 将非字母字符全部替换为空格字符" " 得到一个全小写的纯字母字符串包含有空格字符
         */
        str = str.toLowerCase();// 将字符串中的英文部分的字符全部变为小写
        String regex = "[\\W]+";// 非字母的正则表达式 --\W：表示任意一个非单词字符
        str = str.replaceAll(regex, " ");
        String[] strs = str.split(" "); // 以空格作为分隔符获得字符串数组
        return strs;
    }
    /*
    *查找出现次数最多的单词
     */
    public static void countWord(String[] strs) {
        //从第一个字符串与后面的字符串比较，使用嵌套循环
        int maxValue = 0;//接收最多次数
        String maxStr = null;//最多次数的单词

        for (int i = 0; i <strs.length-1; i++) {
            //中间变量
            int value = 1;
            String str = null;

            for (int j = i+1; j <strs.length ; j++) {
                //字符串比较
                if (strs[i].equals(strs[j])){
                    value ++;
                    str = strs[i];
                }
                //通过这个比较最多次数的大小，确定单词出现次数
                else if (value > maxValue){
                    maxValue = value;
                    maxStr = str;
                }
            }
        }

        System.out.println("出现最多的单词是：" + maxStr + "出现了" + maxValue + "次");
    }

    public static void main(String[] args) {
        String str = "The Apache Tomcat Project is proud to announce the release of version 7.0.105 of Apache Tomcat. This release contains a number of bug fixes and improvements compared to version 7.0.104.";
        str = str.replaceAll("\\d+","");
        String[] strings = strReplace(str);
        countWord(strings);
        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i]);
        }
    }
}