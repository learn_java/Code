package com.qh.work01;

/**
 * 用户异常处理
 */
public class UserException extends RuntimeException{
    public UserException(){

    }
    public UserException(String message){
        super(message);
    }
}
