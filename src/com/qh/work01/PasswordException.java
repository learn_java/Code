package com.qh.work01;

/**
 * 密码异常处理
 */
public class PasswordException extends RuntimeException{
    public PasswordException(){

    }
    public PasswordException(String message){
        super(message);
    }
}
